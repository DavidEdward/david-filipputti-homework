package incrocio;

import javax.swing.*;
import javax.swing.Timer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class GUI extends JPanel{
    Macchina m[]=new Macchina[100];
    int c=0,k=0;
    Incrocio i;

    GUI(Incrocio i) {
        this.i= i;
        Timer t=new Timer(50, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                repaint();
            }
        });
        t.setRepeats(true);
        t.start();
    }

    public void add(Macchina m1){
        m[c]=m1;
        c++;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        g.setColor(Color.BLACK);
        g.drawRect(0,200,420,40);
        g.drawRect(190,0,40,420);
        //g.fillRect(370,370,40,40);

        k=0;

        while(k<c){
            g.fillRect(m[k].getX(),m[k].getY(),10,10);
            k++;
        }
    }
}
